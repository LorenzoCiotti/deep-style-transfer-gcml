# Deep Style Transfer #
La rete è stata modificando partendo dal repository [deep-photo-styletransfer-tf
](https://github.com/LouieYang/deep-photo-styletransfer-tf), La modifica è stata effettuata per permettere di trainare la rete nelle risorse Google Cloud.
La lista delle modifiche è presente al capitolo successivo.

## Modifiche effettuate ##
* E' statro necessario convertire tutte le operazioni di I/O in operazioni native TensorFlow.
* E' stata modificata la struttura delle directory per renderla conforme alle specifiche google cloud.
* E' stata modificata la lista dei parametri in ingresso al fine di utilizzare correttamente l'argomento di *--job-dir* (google cloud infatti vuole che tale argomento sia specificato al momento dell'esecuzione del job ma si aspetta che sia lo script python a gestirlo).
* E' stato necessario modificare il codice in modo che la rete Vgg19 caricasse il file dei pesi dal cloud.
* Sono state modificate le operazioni di salvataggio delle immagini di risultato utilizzando API native tensorflow.
* Sono stati sostituiti i due file di segmentazione presi in ingresso con due immagini nere create dinamicamente al bisogno.
* Sono stati aggiunti due argomenti allo script(*--local* *--remote*) per differenziare le situazioni di esecuzione locale da quelle di esecuzione remota.

## Codice per il lancio ##

### Google Cloud ###
Le operazioni necessarie per eseguire la rete sul cloud google sono presenti nel seguente codice.
E' importante modificare correttamente i path in base alle proprie necessità e configurazioni.

```bash

JOB_NAME="style_transfer_$(date +%Y%m%d_%H%M%S)"
JOB_TYPE=BASIC_GPU
OUTPUT_DIR=gs://computer-vision-for-retails-mlengine/Dataset_Flyingthing_A/Test/Output
CONT_IM_PATH=gs://computer-vision-for-retails-mlengine/Dataset_Flyingthing_A/Test/Census/0006.png
STYLE_IM_PATH=gs://computer-vision-for-retails-mlengine/Dataset_Flyingthing_A/Test/Perfette/0006.png
SERIAL_PATH=gs://computer-vision-for-retails-mlengine/Dataset_Flyingthing_A/Test/Output
NUM_ITER=1000

gcloud ml-engine jobs submit training $JOB_NAME --scale-tier $JOB_TYPE --job-dir $OUTPUT_DIR \
--runtime-version 1.2 --module-name trainer.task --package-path trainer --region europe-west1 \
-- \
--content_image_path $CONT_IM_PATH --style_image_path $STYLE_IM_PATH \
--serial $SERIAL_PATH --max_iter $NUM_ITER --remote \
--style_option 0

```

I seguenti comandi vanno lanciati dalla cartella di root del progetto e si aspettano la struttura consigliata nel link alla pagina di appunti precedente.

### Locale ###
Il comando per lanciare la rete sul proprio pc è il seguente

```bash

OUTPUT_DIR="PATH ALLA DIRECTORY DI OUTPUT"
CONT_IM_PATH="PATH ALL'IMMAGINE DI CONTENT"
STYLE_IM_PATH="PATH ALL'IMMAGINE DI STILE"
SERIAL_PATH="PATH ALLA DIRECTORY IN CUI VERRANNO MEMORIZZATI I RISULTATI INTERMEDI"
NUM_ITER=1000

python task.py --content_image_path $CONT_IM_PATH \
--style_image_path $STYLE_IM_PATH --serial $SERIAL_PATH \
--job-dir $OUTPUT_DIR --max_iter $NUM_ITER --local \
--style_option 2

```

## Risultati ##
Sono stati effettuati dei test utilizzando i valori di *--style_option* 0 e 2 e *NUM_ITER* 1000 e 2000. Entrambi i risultati sono stati insoddisfacenti e la rete si è quindi dimostrata poco valida per l'utilizzo da noi previsto.
In entrambi i test sono stati utilizzati i pesi pretrainati *vgg19.npy*